#include <DHT.h>
#include <SoftwareSerial.h>
#include <stdint.h>

// For DHT
#define DHTPIN 2     
#define DHTTYPE DHT22

// For Sigfox
#define TX 7
#define RX 8

// Init componets
DHT dht(DHTPIN, DHTTYPE);
SoftwareSerial Sigfox(RX, TX);

void setup() {
  // Start serial access to COM and Sigfox
  Serial.begin(9600);
  Sigfox.begin(9600);

  // Init sensors
  dht.begin();
}

void loop() {
   if ((millis()/1000 % 900) == 0) {
    float t = 0.0f;
    float h = 0.0f;
    float hi = 0.0f;
  
    // If no errors happend during measurement send the data
    if (measure(&t, &h, &hi))
      sendData(t, h, hi);

    delay(2000); // prevent multiple calls in the same second
  }

  if (Sigfox.available()) {
    Serial.write(Sigfox.read());
  }

}

// Meaures temperature and humidity
// Additionally computes heat index
// Returns true on success, false on failure
bool measure(float *t, float *h, float *hi) {
  *t = dht.readTemperature();
  *h = dht.readHumidity();

  if (isnan(*t) || isnan(*h)) {
    Serial.println("Failed to read from DHT sensor!");
    return false;
  }

  *hi = dht.computeHeatIndex(*t, *h, false);
  return true;
}

// Sends data to the Sigfox cloud
void sendData(float t, float h, float hi) {
  Serial.println("Sending data to Sigfox cloud");
  String temp = convert(t);
  String hum = convert(h);

  Sigfox.print("AT$SF=");
  Sigfox.print(temp);
  Sigfox.println(hum);

  Serial.println("Done");
}

// Converts a 32 bit float to a string made of 8 hexadecimal chars
String convert(float f) {
  uint32_t mask = 0xF0000000;
  uint8_t tmp;
  String builder = "";
  for (int i = 0; i < 32 / 4; i++) {
    // Bitwise copies the float f into a 32 bit unsigned integer
    // Masks the, to the iteration coresponding, 4 bits
    // Shoves the value into the first 4 bits of the tmp variable
    // tmp should be between 0 to 15
    tmp = (*(uint32_t*)&f & (mask >> i * 4)) >> ((7 - i) * 4);
    
    // Appends the value of tmp as a hex figure
    builder += String(tmp, HEX);
  }
  
  return builder;
}

